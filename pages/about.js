
import {Component} from 'react';
import BaseLayout from '../components/layouts/BaseLayout';
import BasePage from '../components/shared/BasePage'

class About extends Component{
    render(){
        return(
            <BaseLayout {...this.props.auth}>
                <BasePage>
                    <p>Hello About.js</p>
                </BasePage>
            </BaseLayout>
        )
    }
}

export default About;