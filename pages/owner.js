import {Component} from 'react';
import BaseLayout from '../components/layouts/BaseLayout';
import BasePage from '../components/shared/BasePage'
import withAuth from '../components/hoc/withAuth';

class Owner extends Component{
    render(){
        return(
            <BaseLayout {...this.props.auth}>
                <BasePage>
                    <p>Hello Owner page</p>
                </BasePage>
            </BaseLayout>
        )
    }
}

export default withAuth("siteOwner")(Owner);