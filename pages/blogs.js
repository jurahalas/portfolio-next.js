import {Component} from 'react';
import BaseLayout from '../components/layouts/BaseLayout';
import BasePage from '../components/shared/BasePage'

class Index extends Component{
    render(){
        return(
            <BaseLayout {...this.props.auth}>
                <BasePage>
                <p>Hello blog</p>
                </BasePage>
            </BaseLayout>
        )
    }
}

export default Index;