const jwt = require('express-jwt');
const jwksRsa = require('jwks-rsa');

//MIDDLEWARE
exports.checkJWT = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 15,
    jwksUri: 'https://dev-yura.auth0.com/.well-known/jwks.json'
  }),
  audience: 'vG6M73BKiWbNBJxJ7Dfv1nw4cbm47ajN',
  issuer: 'https://dev-yura.auth0.com/',
  algorithms: ['RS256']
});