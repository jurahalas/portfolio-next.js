import React from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import Link from 'next/link'
import auth0 from '../../services/auth0';

const BsNavLink =({ route, title }) => (
    <NavItem className='port-navbar-item'>
        <Link href={ route }>
            <a className='nav-link port-navbar-link'>{ title }</a>
        </Link>
    </NavItem>
);

const Login =() => (
    <NavItem className='port-navbar-item'>
       <span onClick={auth0.login} className='nav-link port-navbar-link clickable'> Login </span>
    </NavItem>
);

const Logout =() => (
    <NavItem className='port-navbar-item'>
      <span onClick={auth0.logout} className='nav-link port-navbar-link clickable'> Logout </span>
    </NavItem>
);

export default class Example extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
      const { isAuthenticated, user } = this.props;

        return (
            <div>
                <Navbar className='port-navbar port-default absolute' color="transparent" dark expand="md">
                    <NavbarBrand className='port-navbar-brand' href="/">Yurii Halas</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <BsNavLink route='/' title='Home'/>
                            <BsNavLink route='/about' title='About'/>
                            <BsNavLink route='/portfolios' title='Portfolio'/>
                            <BsNavLink route='/blogs' title='Blog'/>
                            <BsNavLink route='/cv' title='Cv'/>
                          {
                           isAuthenticated
                              ? <Logout/>
                              : <Login/>
                          }
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}