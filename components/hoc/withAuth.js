import {Component} from 'react';
import BaseLayout from '../layouts/BaseLayout';
import BasePage from '../shared/BasePage';

const namespace = 'http://localhost:3000/';

export default function(role){
  return function(PageComponent) {
    return class withAuth extends Component {

      static async getInitialProps(arg) {
        const pageProps = await PageComponent.getInitialProps && await PageComponent.getInitialProps(arg);

        return {...pageProps};
      }

      renderProtectedPage() {
        const { isAuthenticated, user } = this.props.auth;
        const userRole = user && user[`${namespace}role`];
        let isAuthorized = false;

        if(role) {
          if(userRole && userRole === role) { isAuthorized = true };
        } else {
          isAuthorized = true;
        }

        if(!isAuthenticated) {
          return(
              <BaseLayout {...this.props.auth}>
                <BasePage>
                  <p>You are not authenticated. Please login to access this page</p>
                </BasePage>
              </BaseLayout>
          )
        } else if (!isAuthorized){
          return(
              <BaseLayout {...this.props.auth}>
                <BasePage>
                  <p>You are not authorized. You don't have permission to visit this page</p>
                </BasePage>
              </BaseLayout>
          )
        } else {
          return <PageComponent {...this.props} />
        }
      }

      render() {
        return this.renderProtectedPage()
      }
    }
  }
}
