
export const getCookieFromReq = (req, cookie) => {
  const cookieData = req.headers.cookie.split(';').find(c => c.trim().startsWith(`${cookie}=`));

  if(!cookieData) { return undefined };

  return cookieData.split('=')[1];
};